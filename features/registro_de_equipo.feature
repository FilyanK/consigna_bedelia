#language: es
Característica: Registro de equipo

  @wip
  Escenario: r1 - Registro exitoso de equipo
    Dado hay 0 equipos registrados
    Cuando registro un equipo "River" con presupuesto 300
    Entonces se registra correctamente

  @wip
  Escenario: r2 - Error por exceso de equipos registrados
    Dado hay 10 equipos registrados
    Cuando registro un equipo "River" con presupuesto 300
    Entonces obtengo un error por exceso de equipos

  @wip
  Escenario: r3 - Error por exceso de presupuesto
    Dado registro un equipo "River" con presupuesto 10000000
    Cuando registro un equipo "X" con presupuesto 300
    Entonces obtengo un error por exceso de presupuesto

  @wip
  Escenario: r4 - Error por equipos duplicados
    Dado registro un equipo "River" con presupuesto 300
    Cuando registro un equipo "river" con presupuesto 300
    Entonces obtengo un error por equipo duplicado
